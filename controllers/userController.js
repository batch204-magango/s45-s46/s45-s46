const User = require("../models/User");
const Course = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return `Email exist in the database`
		} else {
			return `User not found`
		}

	})
}



//Controller for User Registration
//new registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result === null){

			if(reqBody.password.length >= 8){
				let newUser = new User({
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				});

				return newUser.save().then((user, error) => {

					if(error) {
						return false
					} else {
						return 'Registered Successfully'
					}
				})
			}
			else{
				return `Password is too weak, try another one`
			}		
		}
		else{
			return `User is already registered`
		}
	})
}



//-------------
// module.exports.registerUser = (reqBody) => {
// 	let newUser = new User ({
// 		firstName: reqBody.firstName,
// 		lastName: reqBody.lastName,
// 		email: reqBody.email,
// 		mobileNo: reqBody.mobileNo,
// 		password: bcrypt.hashSync(reqBody.password, 10)
// 	})



// 	return newUser.save().then((user, error)=> {

// 		if(error) {
// 			return false
// 		} else {
// 			return 'Registered Successfully'
// 		}
// 	})
// }


//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				console.log(result)
				return {
					access:auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Retrieve user details
// module.exports.getUser = (reqParams) => {
// 	return User.findById(reqParams.userId).then(result => {
// 		return result
// 	})
// }

module.exports.getUser = (reqParams) => {


	return User.findById(reqParams.userId).then(result => {
		
		result.password = "";
		return result
	
	})

}





//get all user
// module.exports.getAllUsers = () => {


// 	return User.find({}).then(result => {

// 		for(i = 0; i < result.length; i++)
// 		result[i].password = "";
// 		return result;
	
// 	})

// }


//retrieve profile

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then(result => {
		result.password = ""
		return result
	}).catch(error => "id not found")
}

// module.exports.getUser = (reqParams) => {
// 	return User.findById(reqParams.userId).then(result => {
// 		return result
// 	})
// }


//order
module.exports.order = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then( user => {

			user.orders.push({
				products: {
					productId: data.productId, 
					productName: data.productName,
					price: data.price,
					quantity: data.quantity
				},
				totalAmount: data.price * data.quantity,
			});


			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true	
				}	
			});
		});


		let isProductUpdated = await Product.findById(data.productId).then(product => {
				product.orders.push({userId: data.userId})
				return product.save().then((product, error) => {
					if(error) {
						return false
					} else {
					
						return true
					}
				});
		});

		if(isUserUpdated && isProductUpdated) {
			return true
		} else {
			return false
		}
	}
}

//order 2 solution
// module.exports.order = (reqBody, data) => {
// 	return User.findById(data.userId).then(result => {
// 		if (data.isAdmin === true) { //if admin = true
// 			return false
// 		} else { //if admin = false
// 			let newOrder = new Order ({
// 				userId: reqBody.userId,
// 				products:reqBody.products,
// 				totalAmount: reqBody.totalAmount
// 			});

// 			return newOrder.save().then((order, error) => {
// 				if (error) {
// 					return false
// 				} else {
// 					return "Create New Order"
// 				}
// 			})
// 		}
// 	})
// }