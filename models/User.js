const mongoose = require("mongoose");

const userSchema = new mongoose.Schema ({
	

	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	} ,
	isAdmin: {
		type: String,
		default:false

	}

});

//option 2

// const userSchema = new mongoose.Schema({
// 	email : {
// 		type : String,
// 	},
// 	password : {
// 		type : String,
// 		required : [true, "Password is required"]
// 	},
// 	isAdmin : {
// 		type : Boolean,
// 		default : false
// 	},
// 	orders : [
// 		{
// 			products : [
// 				{
// 					productId : {
// 						type : String
// 					},
// 					productName : {
// 						type: String
// 					},
// 					price : {
// 						type: Number
// 					},
// 					quantity : {
// 						type: Number
// 					}	
// 				}
// 			],	
// 			totalAmount : {
// 				type : Number
// 			},
// 			purchasedOn : {
// 				type : Date,
// 				default : new Date()
// 			}
// 		}
// 	]
	
// })


// const userSchema = new mongoose.Schema({
// 	userId: {
// 		type:String,
// 		default: [true, "User Id is required"]
// 	},
// 	products: [{
// 		productId: {
// 			type: String,
// 			default: [true, "Product Id is required"]
// 		},
// 		quantity: {
// 			type: Number,
// 			default: [true, "Quantity is required"]
// 		}
// 	}],
// 	totalAmount: {
// 		type: Number,
// 		default: [true, "total Amount is required"]
// 	},
// 	purchasedOn: {
// 		type:Date,
// 		default: new Date()
// 	}

// });
module.exports = mongoose.model("User", userSchema);
