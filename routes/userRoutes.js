const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

// //Route for checking if the user's email already exist in the database
router.post("/CheckEmail", (req, res)=> {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

//User Authentication
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for retrieving user
router.get("/:userId", (req, res)=>{
	console.log(req.params)

	userController.getUser(req.params).then(resultFromController => res.send(resultFromController))
})



// router.post("/details", (req, res) =>{

// 		const userData = auth.decode(req.headers.authorization);

// 		console.log(userData);
// 		console.log(req.headers.authorization);

// 	userController.getProfile({userData}).then(resultFromController => res.send(resultFromController))
// });

router.post("/details", auth.verify, (req, res) => {
	console.log(req.headers);

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	
	console.log(userData);
	console.log(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController));

});


//s41 discussion

//Route to create a order
	// router.post("/order", auth.verify, (req, res) => {

	// 	let data = {
	// 		userId: req.body.userId,
	// 		productId: req.body.productId
	// 	}

	// 	userController.enroll(data).then(resultFromController => res.send (
	// 		resultFromController));



	// });

//order
// router.post("/order", auth.verify, (req, res) => {


// 	let data = {

// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId,
// 		totalAmount: parseInt(req.body.totalAmount),
// 		price: parseInt(req.body.price),
// 		quantity: parseInt(req.body.quantity),
// 		productName: req.body.productName,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin

// 	}

// 	userController.order(data).then(resultFromController => res.send(resultFromController));

// });

//solution 2

router.post("/order", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);
	userController.order(req.body2, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
